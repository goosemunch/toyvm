#ifndef __TOYVM_VAR_H__
#define __TOYVM_VAR_H__

#define VARTYPE_NULL 0 // unused
#define VARTYPE_INT 1
#define VARTYPE_FLOAT 2
#define VARLOC_LOCAL 0
#define VARLOC_GLOBAL 1
struct Var
{
    short type; // 1 = int 2 = float
	short loc; // 0 = local 1 = global 
	union
    {
        float f;
        int i;
    };

	// conversion
    float asFloat() const
    {
        if (type == VARTYPE_INT)
            return (float)i;
        return f;
    }

    int asInt() const
    {
        if (type == VARTYPE_FLOAT)
            return (int)f;
        return i;
    }

	// assignments
    void operator=(const int &in)
    {
        type = VARTYPE_INT;
        i = in;
    }

    void operator=(const float &in)
    {
        type = VARTYPE_FLOAT;
        f = in;
    }
	
#define COMP_OVERRIDE(o)									\
	bool operator o (Var &b)								\
	{														\
		if (this->type == b.type)							\
		{													\
			if (this->type == VARTYPE_FLOAT)				\
			{												\
				if (this->f o b.f) return true;				\
			}												\
			else if (this->type == VARTYPE_INT)				\
			{												\
				if (this->i o b.i) return true;				\
			}												\
		}													\
		else												\
		{													\
			if (this->asFloat() o b.asFloat()) return true; \
		}													\
		return false;										\
	} 

	// comparisons
	COMP_OVERRIDE(==)
	COMP_OVERRIDE(<)
	COMP_OVERRIDE(>)
    COMP_OVERRIDE(<=)
    COMP_OVERRIDE(>=)
};

#endif