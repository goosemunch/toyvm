#ifndef __TOYTOKENIZER_H__
#define __TOYTOKENIZER_H__

#include <iostream>
#include <sstream>
#include <string>
#include <vector>

// stl's iostream tokenizer is terrible for programming languages
// so i'm writing my own.
class ToyTokenizer
{
public:
	// for script function calls...
	// a new tokenizer is created using the following
	// constructor
	ToyTokenizer(const char *inBuffer, const int inLength)
		: buffer(inBuffer), bufferLength(inLength), ptr(0)
	{
	}

	bool isValid() const // false if eof reached or something else terrible
	{
		return ptr < bufferLength;
	}
	bool getToken(std::string &result);
	
private:
	int bufferLength;
	const char *buffer;
	int ptr;
};

#endif
