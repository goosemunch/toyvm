#include "ToyTokenizer.h"
#include <string>

bool ToyTokenizer::getToken(std::string &result)
{
	result.clear();
	
	while (ptr < bufferLength)
	{
		const char c = buffer[ptr++];
		
		if (c == '(') // block comments
		{
			while (ptr < bufferLength &&
				buffer[ptr++] != ')')
			{}
		}
		else if (c == '\\') // line comment
		{
			while (ptr < bufferLength &&
				buffer[ptr++] != '\n')
			{}
		}
		else if (c == '\t' || c == ' ' || c == '\n' || c == '\r') // delimit
		{
			if (result.length())
				break;
		}
		else // append
		{
			result += c;
		}
	}

	// maybe the stream ended before we could finish reading the token
	if (!result.empty())
		// just return whatever we have so far
		return true;
	
	return false;
}
