// jlforth.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <iostream>
#include <sstream>
#include <string>
#include <vector>

#include "ToyVM.h"

std::vector<DictEntry> dict;

std::vector< Var > globalVars;
std::vector< std::string> globalNames;

int numPrims; // built-in + added via init
std::vector<char*> userCodes; // where all user defined words go
std::vector<int> userCodeLengths;

void prim_period(ToyVM *vm)
{
    Var *v = vm->popStack();
    if (v)
    {
        if (v->type == VARTYPE_INT)
            std::cout << v->i << std::endl;
        else if (v->type == VARTYPE_FLOAT)
            std::cout << v->f << std::endl;
    }
}

void prim_plus(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        if (v0->type == VARTYPE_INT && v1->type == VARTYPE_INT)
            v1->i += v0->i;
        else if (v0->type == VARTYPE_FLOAT && v1->type == VARTYPE_FLOAT)
            v1->f += v0->f;
        else
        {
            v1->f = v1->asFloat() + v0->asFloat();
            v1->type = VARTYPE_FLOAT;
        }
        vm->pushStack(v1);
    }
}

void prim_mult(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        if (v0->type == VARTYPE_INT && v1->type == VARTYPE_INT)
            v1->i *= v0->i;
        else if (v0->type == VARTYPE_FLOAT && v1->type == VARTYPE_FLOAT)
            v1->f *= v0->f;
        else
        {
            v1->f = v1->asFloat() * v0->asFloat();
            v1->type = VARTYPE_FLOAT;
        }
        vm->pushStack(v1);
    }
}

void prim_eq(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        Var result;
		result = *v0 == *v1;
        vm->pushStack(&result);
    }
}

void prim_less(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
		Var result;
        result = *v0 > *v1;
        vm->pushStack(&result);
    }
}

void prim_greater(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        Var result;
        result = *v0 < *v1;
        vm->pushStack(&result);
    }
}

void prim_lesseq(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        Var result;
        result = *v0 >= *v1;
        vm->pushStack(&result);
    }
}

void prim_greatereq(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        Var result;
        result = *v0 <= *v1;
        vm->pushStack(&result);
    }
}

void prim_save(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        const int i = v0->asInt();
		if (v0->loc == VARLOC_LOCAL)
			vm->user[i] = *v1;
		else if (v0->loc == VARLOC_GLOBAL)
			globalVars[i] = *v1;
    }
}

void prim_load(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    if (v0)
    {
        const int i = v0->asInt();
		if (v0->loc == VARLOC_LOCAL)
			vm->pushStack(&vm->user[i]);
		else if (v0->loc == VARLOC_GLOBAL)
			vm->pushStack(&globalVars[i]);
    }
}

void prim_dup(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    if (v0)
    {
        vm->top++; // keep
        vm->pushStack(v0);
    }
}

void prim_2dup(ToyVM *vm)
{
    Var *v0 = vm->popStack();
	Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        vm->top+=2; // keep
        vm->pushStack(v1);
		vm->pushStack(v0);
    }
}

void prim_pop(ToyVM *vm)
{
    vm->popStack();
}

void prim_rpop(ToyVM *vm)
{
    vm->popRStack();
}

void prim_swap(ToyVM *vm)
{
    Var *v0 = vm->popStack();
    Var *v1 = vm->popStack();
    if (v0 && v1)
    {
        Var temp = *v0;
        *v0 = *v1;
        *v1 = temp;
        vm->top += 2;
    }
}

void prim_tor(ToyVM *vm)
{
	Var *v = vm->popStack();
	if (v) vm->pushRStack(v);
}

void prim_fromr(ToyVM *vm)
{
	Var *v = vm->popRStack();
	if (v) vm->pushStack(v);
}

void prim_fetchr(ToyVM *vm)
{
	Var *v = vm->popRStack();
	if (v) 
	{
		vm->rtop++; // keep
		vm->pushStack(v);
	}
}

void prim_nest(ToyVM *vm)
{
	// user code index on stack
	Var *v0 = vm->popStack();
	if (v0)
	{
		const int userCodeID = v0->asInt();

		// push current ip onto return stack
		Var temp;
		temp = vm->ip;
		temp.loc = vm->uid;
		vm->pushRStack(&temp);

		// prep for next
        vm->changeFrame(userCodeID, 0);
	}
}

void prim_exit(ToyVM *vm)
{
	// pop return stack if not empty.
    // if empty, then must be end of program
    if (vm->rtop > 0)
    {
        Var *ret = vm->popRStack();
        vm->changeFrame(ret->loc, ret->asInt());
    }
}

primDef builtinPrimDefs[] =
{
    { ".", prim_period },
    { "=", prim_eq },
    { "<", prim_less },
    { ">", prim_greater },
    { "<=", prim_lesseq },
    { ">=", prim_greatereq },
    { "+", prim_plus },
    { "*", prim_mult },
    { "@", prim_load },
    { "!", prim_save },
    { "dup", prim_dup },
	{ "2dup", prim_2dup },
    { "pop", prim_pop },
    { "rpop", prim_rpop },
    { "swap", prim_swap },
	{ ">r", prim_tor },
	{ "r>", prim_fromr },
	{ "r@", prim_fetchr },
	{ "exit", prim_exit }
};
const int numBuiltinPrims = sizeof(builtinPrimDefs) / sizeof(primDef);


void toyvm_init(primDef *userPrims, const int numUserPrims)
{
	// put all builtin and user prim words into one list
	dict.reserve(numBuiltinPrims + numUserPrims);
	for (int ctr = 0; ctr < numBuiltinPrims; ctr++)
	{
		DictEntry entry;
		entry.key = builtinPrimDefs[ctr].key;
		entry.func = builtinPrimDefs[ctr].ptr;
		dict.push_back(entry);
	}
	for (int ctr = 0; ctr < numUserPrims; ctr++)
	{
		DictEntry entry;
		entry.key = userPrims[ctr].key;
		entry.func = userPrims[ctr].ptr;
		dict.push_back(entry);
	}
	numPrims = dict.size();

	globalVars.reserve(100);
	globalNames.reserve(100);
	userCodes.reserve(100);
	userCodeLengths.reserve(100);
}

void toyvm_exit()
{
	for (int ctr = 0; ctr < (int)userCodes.size(); ctr++)
		delete [] userCodes[ctr];
	userCodes.clear();
	userCodeLengths.clear();
    dict.clear();
	globalVars.clear();
	globalNames.clear();
}

void ToyVM::changeFrame(const int uid_, const int ip_)
{
    uid = uid_;
    ip = ip_;
    code = userCodes[uid];
    codeLen = userCodeLengths[uid];
}

bool ToyVM::step()
{
    // no more code to run
    if (ip >= codeLen) return false;

    if (code[ip] == JFB_PUSHVAR)
    {
        ip++;
        Var *vtr = (Var *)&code[ip];
        pushStack(vtr);
        ip += sizeof(Var);
    }
	else if (code[ip] == JFB_RUNWORD)
	{
		ip++;
		int *wordid = (int *)&code[ip];
        ip += sizeof(int);
		dict[*wordid].func(this);
	}
    else if (code[ip] == JFB_JUMPEQZ)
    {
        ip++;
        Var *v = popStack();
        Var *off = (Var *)&code[ip];
        ip += sizeof(Var);

        if (v->asInt() == 0)
        {
            ip += off->i;
        }
    }

    return true;
}
