// testforth.cpp : Defines the entry point for the console application.
//

#include <stdio.h>
#include <fstream>

#include "ToyVM.h"


void prim_drand(ToyVM *vm)
{
    Var v;
    v = (float)rand() / (float)RAND_MAX;
    vm->pushStack(&v);
}

primDef userPrimDefs[] =
{
    {"drand", prim_drand }
};
const int numUserPrims = sizeof(userPrimDefs) / sizeof(primDef);


int main(int argc, char* argv[])
{
	ToyVM vm;
    toyvm_init(userPrimDefs, numUserPrims);

	for (int ctr =0; ctr < 3; ctr++)
	{
		std::string filename;
		if (ctr == 0) filename = "math.txt";
		else if (ctr == 1) filename = "script.txt";
		else if (ctr == 2) filename = "script2.txt";

		std::cout << "run -- " << filename << std::endl;

		ScriptInfo info;

		// read script
		// compile
		std::ifstream file(filename);
		if (file.is_open())
		{
			file.seekg(0, std::ios::end);
			std::streampos size = file.tellg();
			std::string script((int)size, ' ');
			file.seekg(0);
			file.read(&script[0], (int)size);

			// script namespace: script name without extension
			std::string ns;
			std::size_t found = filename.rfind('.');
			if (found == std::string::npos)
				ns = filename;
			else
				ns = filename.substr(0, found);
			std::cout << "namespace: " << ns << std::endl;
			char code[16384];
			toyvm_compile(code, info, ns, script);
		}

		for (int ctr = 0; ctr < (int)info.functionNames.size(); ctr++)
		{
			if (info.functionNames[ctr] == "main")
			{
				vm.run(info.functionIndices[ctr]);
				break;
			}
		}
	}

	toyvm_exit();
	return 0;
}

