#ifndef __TOYVM_INTERNAL_H__
#define __TOYVM_INTERNAL_H__

#include <vector>
#include <string>
#include <iostream>
#include "ToyVMVar.h"


// bytecodes
#define JFB_PUSHVAR 1 // followed by var
#define JFB_RUNWORD 2 // followed by var (i: dictionary entry index)
#define JFB_JUMPEQZ 4 // jump if top of stack is 0

#define MAX_STACK 64
#define MAX_USER 16

struct ToyVM
{
    const char *code;
    int codeLen;

    int uid; // currently running user code index
    int ip;
    int top;
    int rtop;
    Var stack[MAX_STACK];
    Var rstack[MAX_STACK];

    Var user[MAX_USER];

    ToyVM()
    {
        reset();
    }

	void reset()
	{
		ip = 0;
        top = 0;
        rtop = 0;
        uid = -1;
		for (int i = 0; i < MAX_USER; i++)
			user[i] = 0;
	}

    void pushStack(Var *v)
    {
        if (top >= MAX_STACK)
        {
            std::cout << "ERR: stack overflow\n";
            return;
        }
        stack[top] = *v;
        top++;
    }

    Var *popStack()
    {
        if (!top)
        {
            std::cout << "ERR: stack underflow\n";
            return NULL;
        }
        return &stack[--top];
    }

    void pushRStack(Var *v)
    {
        if (rtop >= MAX_STACK)
        {
            std::cout << "ERR: return stack overflow\n";
            return;
        }
        rstack[rtop] = *v;
        rtop++;
    }

    Var *popRStack()
    {
        if (!rtop)
        {
            std::cout << "ERR: return stack underflow\n";
            return NULL;
        }
        return &rstack[--rtop];
    }

    void changeFrame(const int uid_, const int ip_);

    // run one instruction
    bool step();

	void run (const int in_uid)
	{
        changeFrame(in_uid, 0);
		while (step())
        {
		}
	}
};

typedef void(*primptr)(ToyVM *);

struct DictEntry
{
    std::string key;
	primptr func;

};

#endif