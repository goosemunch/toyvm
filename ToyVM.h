#ifndef __TOYVM_H__
#define __TOYVM_H__

#include "ToyVMVar.h"
#include "ToyVMinternal.h"
#include <vector>
#include <string>


struct primDef
{
    char *key;
    primptr ptr;
};

// per-VM. if multiple scripts share same VM, then it must share same ScriptInfo as well
// (even though it may end up with duplicate function name entries in the info)
struct ScriptInfo
{
    std::vector<std::string> userNames;

    std::vector<std::string> functionNames; // w/o namespaces
    std::vector<int> functionIndices; // into dict.
};

int toyvm_compile(char *resultCode, ScriptInfo &info,
    const std::string &scriptName, std::string &script);

void toyvm_init(primDef *userPrims = NULL, const int numUserPrims = 0);
void toyvm_exit();

#endif