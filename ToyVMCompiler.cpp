#include <sstream>
#include "ToyVM.h"
#include "ToyTokenizer.h"

extern std::vector<DictEntry> dict;
extern std::vector< Var > globalVars;
extern std::vector< std::string> globalNames;
extern std::vector<char*> userCodes;
extern std::vector<int> userCodeLengths;
extern int numPrims;

void prim_nest(ToyVM *vm);


// 0 if not a number 1 if int. 2 if float
inline int isNum(const char* in)
{
	bool hasDigits = false;
	int result = 1;
	if (*in == '-') in++;
	while (*in)
	{
		if (*in < '0' || *in > '9')
		{
			if (result == 1 && *in == '.')
				result = 2;
			else if (hasDigits && *in == 'f' && *(in+1) == 0)
				result = 2;
			else
				return 0;
		}
		else
		{
			hasDigits = true;
		}
		in++;
	}
	if (!hasDigits) return 0;
	return result;
}

void encodeWord(char *code, int &ip, const char *word)
{
	for (int ctr = 0; ctr < (int)dict.size(); ctr++)
	{
		if (dict[ctr].key == word)
		{
			code[ip++] = JFB_RUNWORD;
			int *v = (int *)&code[ip];
			*v = ctr;
			ip += sizeof(int);
			return;
		}
	}
	std::cout << "ERR: trying to encode unknown word\n";
}

// NOTE do not use for encoding variable/instruction addresses
// (global/local/uid are not passed along)
template <typename T>
void encodeValue(char *code, int &ip, const T val)
{
    code[ip++] = JFB_PUSHVAR;
    Var *v = (Var *)&code[ip];
    *v = val;
    ip += sizeof(Var);
}

// returns code length
int compile(char *code, ScriptInfo &info, 
			const std::string &ns, const std::string &in)
{
    int ip = 0;

    std::vector<int> jmps; // placeholders

	ToyTokenizer tokenizer(&in[0], in.length());
    std::string word;
    while (tokenizer.getToken(word))
    {
        if (!word.length()) continue;
        int numtype = isNum(word.c_str());

        if (numtype)
        {
            if (numtype == VARTYPE_INT) encodeValue(code, ip, (int)atoi(word.c_str()));
            else if (numtype == VARTYPE_FLOAT) encodeValue(code, ip, (float)atof(word.c_str()));
        }
        else if (word == "if")
        {
            code[ip++] = JFB_JUMPEQZ;
            jmps.push_back(ip);
            ip += sizeof(Var);
        }
        else if (word == "endif" || word == "then")
        {
            const int temp = jmps.back();
            jmps.pop_back();

            Var *v = (Var *)&code[temp];
            v->type = VARTYPE_INT;
            v->i = ip - temp - sizeof(Var);
        }
        else if (word == "else")
        {
            const int temp = jmps.back();
            // instruction to always jump: push 0 and jump
            encodeValue(code, ip, 0);
            code[ip++] = JFB_JUMPEQZ;
            jmps.back() = ip; // placeholder
            ip += sizeof(Var);

            Var *v = (Var *)&code[temp];
            v->type = VARTYPE_INT;
            v->i = ip - temp - sizeof(Var);
        }
		else if (word == "do")
		{
			// make copies to return stack
			encodeWord(code, ip, "2dup");
			encodeWord(code, ip, ">r");
			encodeWord(code, ip, ">r");
			// check valid range
			encodeWord(code, ip, "<");
			code[ip++] = JFB_JUMPEQZ;
            jmps.push_back(ip);
            ip += sizeof(Var);
		}
		else if (word == "loop")
		{
			// copy top 2 return stack elements, but increment the top one by 1:
			encodeWord(code, ip, "r>");
            encodeValue(code, ip, 1);
			encodeWord(code, ip, "+");
			encodeWord(code, ip, "r>");
			// copy incremented results back to return stack
			encodeWord(code, ip, "2dup");
			encodeWord(code, ip, ">r");
			encodeWord(code, ip, ">r");

			// check valid range
			encodeWord(code, ip, "<");

			// jump 18 bytes ahead (pushvar(1)+value0(8)+jeqz(1)+addr(8))
			const int skipAlwaysJump = 1+1+sizeof(Var) + sizeof(Var);
			code[ip++] = JFB_JUMPEQZ;
			Var *v = (Var *)&code[ip];
            *v = skipAlwaysJump; 
            ip += sizeof(Var);

			// instruction to always jump: push 0 and jump
            encodeValue(code, ip, 0);
            code[ip++] = JFB_JUMPEQZ;
            v = (Var *)&code[ip];
			ip += sizeof(Var);
			const int backToDo = ip - (jmps.back() + sizeof(Var)); // temp loc + size of the loc
            *v = -backToDo;
			// record do-skipahead (for invalid range)
			v = (Var *)&code[jmps.back()];
			*v = ip - (int)jmps.back() - (int)sizeof(Var);
			jmps.pop_back();

			// pop 2 return stack
			encodeWord(code, ip, "rpop");
			encodeWord(code, ip, "rpop");
		}
        else
        {
            // search words
			const std::string lword = ns + ":" + word;
            int ctr;
            for (ctr = 0; ctr < (int)dict.size(); ctr++)
            {
                if (lword == dict[ctr].key ||
					word == dict[ctr].key)
                {
					// if dictionary entry is a primitive, run it directly.
					if (ctr < numPrims)
					{
						code[ip++] = JFB_RUNWORD;
						int *did = (int *)&code[ip];
						*did = ctr;
						ip += sizeof(int);
					}
					else // else, it must be user-defined word. push user code index onto stack and nest.
					{
						const int userCodeID = ctr-numPrims;
                        encodeValue(code, ip, userCodeID);

						code[ip++] = JFB_RUNWORD;
						int *did = (int *)&code[ip];
						*did = ctr;
						ip += sizeof(int);
					}
					break;
                }
            }
            if (ctr == dict.size())
            {
				// search global
				for (ctr = 0; ctr < (int)globalNames.size(); ctr++)
                {
                    if (word == globalNames[ctr])
                        break;
                }
				if (ctr != globalNames.size())
				{
					code[ip++] = JFB_PUSHVAR;
					Var *v = (Var *)&code[ip];
					v->type = VARTYPE_INT;
					v->loc = VARLOC_GLOBAL;
					v->i = ctr;
					ip += sizeof(Var);
				}
				else
				{
					// search local
					for (ctr = 0; ctr < (int)info.userNames.size(); ctr++)
					{
						if (word == info.userNames[ctr])
							break;
					}
					if (ctr != info.userNames.size())
					{
						code[ip++] = JFB_PUSHVAR;
						Var *v = (Var *)&code[ip];
						v->type = VARTYPE_INT;
						v->loc = VARLOC_LOCAL;
						v->i = ctr;
						ip += sizeof(Var);
					}
					else
					{
						std::cout << "unknown key\n";
						return 0;
					}
				}
            }
        }
    }

	// insert exit 
	encodeWord(code, ip, "exit");

    return ip;
}

// script is composed of user variable declarations and word definitions.
// scriptname becomes the namespace of word/dictionary declarations
int toyvm_compile(char *resultCode, ScriptInfo &info, 
    const std::string &ns, std::string &script)
{
    // for resulting dict def
    char code[16384];

    //DictEntry newWord;
	std::string wordname;
    
    std::string fragment; // code for dict definitions
    std::string rest; // global code
    
	/*
	0: start
	1: word definition
	2: 
	3: global var declaration
	4: local var declaration
	*/
    int mode = 0;

	ToyTokenizer tokenizer(&script[0], script.length());
    std::string token;
    while (tokenizer.getToken(token))
    {
        if (mode == 0)
        {
            if (token == ":")
            {
                fragment = "";
                wordname = "";
                mode = 1;
            }
			else if (token == "global" || token == "variable")
            {
                mode = 3;
            }
            else if (token == "local" || token == "user")
            {
                mode = 4;
            }
            else
            {
                rest += " ";
                rest += token;
            }
        }
        else if (mode == 1)
        {
            if (wordname == "")
            {
                if (token == ";")
                {
                    // illegal word name.
                    mode = 0;
                    continue;
                }
                wordname = token;
            }
            else if (token == ";")
            {
				DictEntry newWord;
				newWord.key = ns + ":" + wordname;
				newWord.func = prim_nest;

				const int len = compile(code, info, ns, fragment);
                if (len)
                {
					// check if key already exists
				    int ctr;
					for (ctr = 0; ctr < (int)dict.size(); ctr++)
					{
						if (dict[ctr].key == newWord.key) break;
					}

					const int userCodeID = ctr - numPrims;
					
					if (ctr == dict.size()) // not found. insert new entry
					{
						char * newCode = new char[len];
						memcpy(newCode, code, len);
						userCodes.push_back(newCode);
						userCodeLengths.push_back(len);
						dict.push_back(newWord);

						info.functionNames.push_back(wordname);
						info.functionIndices.push_back(userCodeID);
					}
					else // override existing entry
					{
						delete [] userCodes[userCodeID];
						userCodes[userCodeID] = new char[len];
						userCodeLengths[userCodeID] = len;
						memcpy(userCodes[userCodeID], code, len);
					}
                }
                mode = 0;
                continue;
            }
            else
            {
                fragment += " ";
                fragment += token;
            }
        }
		else if (mode == 3)
		{
			// see if global variable name is already declared
			int ctr;
			for (ctr = 0; ctr < (int)globalNames.size(); ctr++)
            {
                if (globalNames[ctr] == token)
                    break;
            }
			
			// if not declared already, make entry
			if (ctr == globalNames.size())
			{
				Var temp;
				temp = 0;
				globalVars.push_back(temp);
				globalNames.push_back(token);
			}
			mode = 0;
		}
        else if (mode == 4)
        {
            // check for redeclaration
            int ctr;
            for (ctr = 0; ctr < (int)info.userNames.size(); ctr++)
            {
                if (info.userNames[ctr] == token)
                    break;
            }
            if (ctr == info.userNames.size())
            {
                info.userNames.push_back(token);
            }
            mode = 0;
        }
	}

    return compile(resultCode, info, ns, rest);
}